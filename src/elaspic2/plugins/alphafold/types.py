from pathlib import Path
from typing import Any, Dict, List, NamedTuple, Optional, Union


class AlphaFoldData(NamedTuple):
    sequence: str
    ligand_sequence: Optional[str]
    msa: List[str]
    structure_file: Union[str, Path]
    predictions: Dict[str, Any]


class AlphaFoldBuildError(Exception):
    """Error building data for Alphafold"""


class AlphaFoldAnalyzeError(Exception):
    """Error analyzing mutations using Alphafold"""
