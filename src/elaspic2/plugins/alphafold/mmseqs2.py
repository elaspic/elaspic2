import contextlib
import random
import tarfile
import tempfile
import time
from pathlib import Path

import requests  # type: ignore
from requests.adapters import HTTPAdapter  # type: ignore
from tqdm.auto import tqdm
from urllib3.util.retry import Retry

TQDM_BAR_FORMAT = "{l_bar}{bar}| {n_fmt}/{total_fmt} [elapsed: {elapsed} remaining: {remaining}]"

MMSEQS2_HOST_URL = "https://a3m.mmseqs.com"


@contextlib.contextmanager
def api_gateway(site=MMSEQS2_HOST_URL, *args, **kwargs):
    # Sorry for overusing your API... :(
    from requests_ip_rotator import ApiGateway

    gateway = ApiGateway(site, *args, **kwargs)
    gateway.max_retries = Retry(total=5, backoff_factor=0.1, status_forcelist=[500, 502, 503, 504])
    try:
        gateway.start()
        yield gateway
    finally:
        gateway.shutdown()


def run_mmseqs2(
    seqs,
    *,
    use_env=True,
    use_filter=True,
    host_url=MMSEQS2_HOST_URL,
    proxies=None,
    gateway=None,
):
    """Use the MMseqs2 API to obtain alignment(s) for query sequence(s).

    Adapted from: https://github.com/sokrypton/ColabFold/blob/main/beta/colabfold.py

    Examples:

    ```
    proxies = {
        "http": "http://108.62.49.26:3128",
        "https": "https://3.213.139.74:8888",
    }
    run_mmseqs2("AAA", procies=proxies)
    ```
    """

    def get_http_adapter():
        retries = Retry(total=5, backoff_factor=0.1, status_forcelist=[500, 502, 503, 504])
        return HTTPAdapter(max_retries=retries)

    session = requests.Session()
    if gateway is not None:
        session.mount(host_url, gateway)
    else:
        session.mount(host_url, get_http_adapter())

    if proxies is not None:
        session_down = requests.Session()
        session_down.mount(host_url, get_http_adapter())
    else:
        session_down = session

    def submit(seqs, mode, N=101):
        n, query = N, ""
        for seq in seqs:
            query += f">{n}\n{seq}\n"
            n += 1

        res = session.post(f"{host_url}/ticket/msa", data={"q": query, "mode": mode})
        try:
            out = res.json()
        except ValueError:
            out = {"status": "UNKNOWN"}
        return out

    def status(ID):
        res = session.get(f"{host_url}/ticket/{ID}")
        try:
            out = res.json()
        except ValueError:
            out = {"status": "UNKNOWN"}
        return out

    def download(ID, tar_gz_file):
        result_url = f"{host_url}/result/download/{ID}"
        res = session_down.get(result_url, proxies=proxies)
        with tar_gz_file.open("wb") as out:
            out.write(res.content)

    if isinstance(seqs, str):
        seqs = [seqs]
        is_single_seq = True
    else:
        is_single_seq = False

    # setup mode
    if use_filter:
        mode = "env" if use_env else "all"
    else:
        mode = "env-nofilter" if use_env else "nofilter"

    # define path
    tmp_dir = tempfile.TemporaryDirectory()
    path = Path(tmp_dir.name)

    # call mmseqs2 api
    tar_gz_file = path.joinpath("out.tar.gz")
    N, REDO = 101, True

    # lets do it!
    if not tar_gz_file.is_file():
        TIME_ESTIMATE = 150 * len(seqs)
        with tqdm(total=TIME_ESTIMATE, bar_format=TQDM_BAR_FORMAT) as pbar:
            while REDO:
                pbar.set_description("SUBMIT")

                time.sleep(random.randint(0, 60))
                # Resubmit job until it goes through
                out = submit(seqs, mode, N)
                while out["status"] in ["UNKNOWN", "RATELIMIT"]:
                    # resubmit
                    pbar.set_description(out["status"])
                    time.sleep(random.randint(0, 60))
                    out = submit(seqs, mode, N)

                # wait for job to finish
                ID, TIME = out["id"], 0
                pbar.set_description(out["status"])
                while out["status"] in ["UNKNOWN", "RUNNING", "PENDING"]:
                    t = random.randint(0, 60)
                    time.sleep(t)
                    out = status(ID)
                    pbar.set_description(out["status"])
                    if out["status"] == "RUNNING":
                        TIME += t
                        pbar.update(n=t)
                    # if TIME > 900 and out["status"] != "COMPLETE":
                    #  # something failed on the server side, need to resubmit
                    #  N += 1
                    #  break

                if out["status"] == "COMPLETE":
                    if TIME < TIME_ESTIMATE:
                        pbar.update(n=(TIME_ESTIMATE - TIME))
                    REDO = False

                if out["status"] == "ERROR":
                    REDO = False
                    raise Exception(
                        "MMseqs2 API is giving errors. Please confirm your input is a valid "
                        "protein sequence. If error persists, please try again an hour later."
                    )

            # Download results
            time.sleep(2)
            download(ID, tar_gz_file)

    # prep list of a3m files
    a3m_files = [path.joinpath("uniref.a3m")]
    if use_env:
        a3m_files.append(path.joinpath("bfd.mgnify30.metaeuk30.smag30.a3m"))

    # extract a3m files
    if not a3m_files[0].is_file():
        try:
            with tarfile.open(tar_gz_file) as tar_gz:
                tar_gz.extractall(path)
        except Exception as e:
            import shutil

            temporary_output = f"/tmp/{tar_gz_file.name}"
            shutil.copy(tar_gz_file, temporary_output)
            print(f"Error: {e!r}! URL: {host_url}/result/download/{ID} File: {temporary_output!r}")
            raise

    # gather a3m lines
    a3m_lines: dict[int, list[str]] = {}
    for a3m_file in a3m_files:
        update_M, M = True, -1
        for line in open(a3m_file, "r"):
            if not line:
                continue
            if "\x00" in line:
                line = line.replace("\x00", "")
                update_M = True
            if line.startswith(">") and update_M:
                M = int(line[1:].rstrip())
                update_M = False
            a3m_lines.setdefault(M, []).append(line)

    alignments = [a3m_lines[N + i] for i in range(len(a3m_lines))]

    if is_single_seq:
        assert len(alignments) == 1
        return alignments[0]
    else:
        return alignments
