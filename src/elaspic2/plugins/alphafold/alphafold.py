import logging
import subprocess
from pathlib import Path
from typing import Any, Dict, List, Optional, Union

import haiku as hk
import jax
import numpy as np
from kmtools.structure_tools.types import DomainMutation as Mutation

import elaspic2.plugins.alphafold.data
from elaspic2.core import MutationAnalyzer, SequenceTool
from elaspic2.plugins.alphafold import templates
from elaspic2.plugins.alphafold.types import AlphaFoldAnalyzeError, AlphaFoldData

logger = logging.getLogger(__name__)


class AlphaFold(SequenceTool, MutationAnalyzer):
    model_params = None
    model_config = None
    model = None
    is_loaded: bool = False

    @classmethod
    def load_model(cls, model_name="model_1", heads_to_remove=(), device=None) -> None:
        from alphafold.model import config, data, model

        class PatchedRunModel(model.RunModel):
            def __init__(self, config, params=None):
                self.config = config
                self.params = params

                def _forward_fn(batch):
                    alphafold_model = model.modules.AlphaFold(self.config.model)
                    return alphafold_model(
                        batch,
                        is_training=False,
                        compute_loss=False,
                        ensemble_representations=True,
                        return_representations=True,
                    )

                self.apply = jax.jit(hk.transform(_forward_fn).apply)
                self.init = jax.jit(hk.transform(_forward_fn).init)

        if device not in ["cpu", "gpu", "tpu"]:
            raise ValueError(f"Unsupported device type: {device!r}.")
        jax.config.update("jax_platform_name", device)

        data_dir = cls._get_data_dir()
        cls._download_model_data(data_dir)

        model_params = data.get_model_haiku_params(
            model_name=model_name + "_ptm", data_dir=str(data_dir)
        )
        cls.model_params = model_params

        model_config = config.model_config(model_name + "_ptm")
        model_config.data.eval.num_ensemble = 1
        model_config.model.num_recycle = 0  # AS
        for head in heads_to_remove:
            del model_config.model.heads[head]
        cls.model_config = model_config

        cls.model = PatchedRunModel(model_config, model_params)

        cls.is_loaded = True

    @staticmethod
    def _get_data_dir():
        return Path(elaspic2.plugins.alphafold.data.__path__[0])

    @classmethod
    def _download_model_data(cls, data_dir: Optional[Path] = None):
        # This helps when downloading module data while building the Docker image
        if data_dir is None:
            data_dir = cls._get_data_dir()

        data_dir.mkdir(exist_ok=True)

        required_files = [
            data_dir.joinpath("params", f"params_model_{i}_ptm.npz") for i in range(1, 6)
        ] + [data_dir.joinpath("params", f"params_model_{i}.npz") for i in range(1, 6)]

        if not all([file.is_file() for file in required_files]):
            logger.info("Downloading AlphaFold model files. This may take several minutes...")
            url = "https://storage.googleapis.com/alphafold/alphafold_params_2021-07-14.tar"
            system_command = f"curl -fsSL {url} | tar -x"
            subprocess.run(system_command, shell=True, cwd=data_dir.joinpath("params"), check=True)

    @classmethod
    def build(  # type: ignore[override]
        cls,
        sequence: str,
        ligand_sequence: Optional[str],
        msa: List[str],
        structure_file: Union[str, Path, None] = None,
        remove_hetatms=True,
    ) -> AlphaFoldData:
        """

        Args:
            query_name: "PDBid_chain" format where PDBid is 4 letter alphabet and chain
            is alphanumeral with no length limit
        """
        if ligand_sequence is not None:
            raise NotImplementedError("Interactions are not yet implemented.")

        if remove_hetatms:
            sequence = sequence.replace("X", "")

        processed_feature_dict = cls._create_feature_dict(sequence, msa, structure_file)

        predictions = cls.model.predict(processed_feature_dict)

        return AlphaFoldData(
            sequence=sequence,
            ligand_sequence=ligand_sequence,
            msa=msa,
            structure_file=structure_file,
            predictions=predictions,
        )

    @classmethod
    def analyze_mutation(cls, mutation: str, data: AlphaFoldData) -> dict:
        mut = Mutation.from_string(mutation)

        if data.sequence[int(mut.residue_id) - 1] != mut.residue_wt:
            raise AlphaFoldAnalyzeError(
                f"Mutation does not match sequence ({mut}, {data.sequence})."
            )

        def mutate_sequence(sequence, mut):
            aa_list = list(sequence)
            assert aa_list[mut.residue_id - 1] == mut.residue_wt
            aa_list[mut.residue_id - 1] = mut.residue_mut
            sequence_mut = "".join(aa_list)
            assert sequence != sequence_mut
            return sequence_mut

        sequence_mut = mutate_sequence(data.sequence, mut)
        processed_feature_dict_mut = cls._create_feature_dict(
            sequence_mut, data.msa, data.structure_file
        )
        predictions_mut = cls.model.predict(processed_feature_dict_mut)

        results_wt = {
            f"{key}_wt": value
            for key, value in cls._predictions_to_embeddings(
                data.predictions, mut.residue_id - 1
            ).items()
        }
        results_mut = {
            f"{key}_mut": value
            for key, value in cls._predictions_to_embeddings(
                predictions_mut, mut.residue_id - 1
            ).items()
        }
        return results_wt | results_mut

    @classmethod
    def _create_feature_dict(
        cls, sequence: str, msa: List[str], structure_file: Union[str, Path], random_seed: float = 0
    ) -> Dict[str, Any]:
        from alphafold.data import pipeline

        msa, deletion_matrix = pipeline.parsers.parse_a3m("".join(msa))
        feature_dict = pipeline.make_sequence_features(
            sequence=sequence, description="none", num_res=len(sequence)
        ) | pipeline.make_msa_features(msas=[msa], deletion_matrices=[deletion_matrix])
        if structure_file is None:
            feature_dict |= templates.make_mock_template_features(sequence)
        else:
            feature_dict |= templates.extract_template_features(sequence, structure_file)
        processed_feature_dict = cls.model.process_features(feature_dict, random_seed=random_seed)
        return processed_feature_dict

    @staticmethod
    def _predictions_to_embeddings(predictions, idx: int) -> Dict[str, Union[float, np.ndarray]]:
        assert idx >= 0

        def as_residue(x):
            return x[idx].to_py()

        def as_protein(x):
            return x.mean(axis=0).to_py()

        embeddings = {
            "experimentally_resolved": predictions["experimentally_resolved"]["logits"],
            "predicted_lddt": predictions["predicted_lddt"]["logits"],
            "msa_first_row": predictions["representations"]["msa_first_row"],
            "single": predictions["representations"]["single"],
            "structure_module": predictions["representations"]["structure_module"],
        }

        output = {
            "scores_residue_plddt": predictions["plddt"][idx].item(),
            "scores_protein_plddt": predictions["plddt"].mean(axis=0).item(),
            "scores_protein_max_predicted_aligned_error": predictions[
                "max_predicted_aligned_error"
            ].item(),
            "scores_proten_ptm": predictions["ptm"].item(),
            **{f"features_residue_{key}": as_residue(value) for key, value in embeddings.items()},
            **{f"features_protein_{key}": as_protein(value) for key, value in embeddings.items()},
        }

        return output
