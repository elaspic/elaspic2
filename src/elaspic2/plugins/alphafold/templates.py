import tempfile
from pathlib import Path
from typing import Dict, Optional, Union

import gemmi
import numpy as np

from elaspic2.plugins.alphafold.types import AlphaFoldBuildError


def prepare_structure_for_alphafold(structure_file: Union[str, Path]):
    structure = gemmi.read_structure(structure_file)
    structure.setup_entities()
    structure.assign_label_seq_id()

    model = structure[0]
    model.name = "1"

    assert len(structure.entities) == 1

    groups = gemmi.MmcifOutputGroups(True)
    groups.group_pdb = True

    block = structure.make_mmcif_block(groups)

    block.set_pair("_pdbx_audit_revision_history.revision_date", "2100-01-01")

    col = block.find("_chem_comp.", ["id", "type"]).find_column("type")
    for i in range(len(col)):
        col[i] = "'L-peptide linking'"

    with tempfile.NamedTemporaryFile(suffix=".cif") as tmp_file:
        block.write_file(tmp_file.name)
        with open(tmp_file.name, "rt") as fin:
            cif_string = fin.read()

    return structure.name, structure.entities[0].name, cif_string


def extract_template_features(
    sequence: str,
    structure_file: Union[str, Path],
    mapping: Optional[Dict[int, int]] = None,
):
    """
    Adapted from: https://github.com/deepmind/alphafold/blob/0be2b30b98f0da7aecb973bde04758fae67eb913/alphafold/data/templates.py#L485
    """  # noqa: D301,E501
    from alphafold.common import residue_constants
    from alphafold.data import mmcif_parsing, templates

    if mapping is None:
        mapping = {i: i for i in range(len(sequence))}

    if structure_file.endswith(".cif"):
        structure_file = Path(structure_file)
        pdb_id = structure_file.stem
        chain_id = "A"
        with structure_file.open("rt") as fin:
            cif_string = fin.read()
    else:
        pdb_id, chain_id, cif_string = prepare_structure_for_alphafold(structure_file)

    parsing_result = mmcif_parsing.parse(file_id=pdb_id, mmcif_string=cif_string)
    mmcif_object = parsing_result.mmcif_object

    chain_sequence = mmcif_object.chain_to_seqres[chain_id]
    for query_idx, target_idx in mapping.items():
        if (
            query_idx >= len(sequence)
            or target_idx >= len(chain_sequence)
            or sequence[query_idx] != chain_sequence[target_idx]
        ):
            raise AlphaFoldBuildError("Could not find query sequence in template")

    try:
        # Essentially set to infinity - we don't want to reject templates unless
        # they're really really bad.
        all_atom_positions, all_atom_mask = templates._get_atom_positions(
            mmcif_object, chain_id, max_ca_ca_distance=1500.0
        )
    except (templates.CaDistanceError, KeyError) as error:
        raise AlphaFoldBuildError(f"Could not get atom data for structure {pdb_id}") from error

    all_atom_positions = np.split(all_atom_positions, all_atom_positions.shape[0])
    all_atom_masks = np.split(all_atom_mask, all_atom_mask.shape[0])

    output_templates_sequence = []
    templates_all_atom_positions = []
    templates_all_atom_masks = []

    for _ in sequence:
        # Residues in the sequence that are not in the template:
        templates_all_atom_positions.append(np.zeros((residue_constants.atom_type_num, 3)))
        templates_all_atom_masks.append(np.zeros(residue_constants.atom_type_num))
        output_templates_sequence.append("-")

    for k, v in mapping.items():
        template_index = v
        templates_all_atom_positions[k] = all_atom_positions[template_index][0]
        templates_all_atom_masks[k] = all_atom_masks[template_index][0]
        output_templates_sequence[k] = chain_sequence[v]

    # Alanine (AA with the lowest number of atoms) has 5 atoms (C, CA, CB, N, O).
    if np.sum(templates_all_atom_masks) < 5:
        raise AlphaFoldBuildError(
            f"Template all atom mask was all zeros: {pdb_id}_{chain_id}. "
            f"Residue range: {min(mapping.values())}-{max(mapping.values())}"
        )

    output_templates_sequence = "".join(output_templates_sequence)

    templates_aatype = residue_constants.sequence_to_onehot(
        output_templates_sequence, residue_constants.HHBLITS_AA_TO_ID
    )

    template_features = {
        "template_all_atom_positions": np.array(templates_all_atom_positions)[None],
        "template_all_atom_masks": np.array(templates_all_atom_masks)[None],
        "template_sequence": [output_templates_sequence.encode()],
        "template_aatype": np.array(templates_aatype)[None],
        "template_domain_names": [f"{pdb_id.lower()}_{chain_id}".encode()],
        "template_release_date": [b"none"],
    }
    return template_features


def make_mock_template_features(sequence: str):
    """

    Adapted from: https://github.com/sokrypton/ColabFold
    """
    from alphafold.data import templates

    size = len(sequence)
    output_templates_sequence = "-" * size
    output_confidence_scores = np.full(size, -1)
    templates_all_atom_positions = np.zeros((size, templates.residue_constants.atom_type_num, 3))
    templates_all_atom_masks = np.zeros((size, templates.residue_constants.atom_type_num))
    templates_aatype = templates.residue_constants.sequence_to_onehot(
        output_templates_sequence, templates.residue_constants.HHBLITS_AA_TO_ID
    )
    template_features = {
        "template_all_atom_positions": templates_all_atom_positions[None],
        "template_all_atom_masks": templates_all_atom_masks[None],
        "template_sequence": [b"none"],
        "template_aatype": np.array(templates_aatype)[None],
        "template_confidence_scores": output_confidence_scores[None],
        "template_domain_names": [b"none"],
        "template_release_date": [b"none"],
    }
    return template_features
